#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(int argc, char const *argv[])
{
  map <string, int> map_color;
  map <string, int> map_company;

  int N;  cin >> N;
  int index_company = 0 , index_color = 0;
  int input_color[1000], input_company[1000], input_size[1000]; bool input_type[1000];
  for ( int i=0; i<N; i++)
  {
    string company;  string color; char type;
    cin >> company >> input_size[i] >> color >> type;

    //Assign Index of New Company
    if ( map_company.find(company) == map_company.end())
      {
        map_company[company] = index_company;
        index_company++;
      }
      //Assign Index of New Color
    if ( map_color.find(color) == map_color.end())
      {
        map_color[color] = index_color;
        index_color++;
      }

    input_company[i] = map_company[company];
    input_color[i] = map_color[color];
    if ( type == 'L') input_type[i] = true;
    else  input_type[i] = false;
  }

  int colors[index_company][index_color][16][2];
  //Initliazing array with Zeros
  for (int i=0; i<index_company; i++)
    for ( int j=0; j<index_color; j++)
      for ( int k=0; k<16; k++)
        for ( int l =0; l<2; l++ )
          colors[i][j][k][l] = 0;

  //REading the input again
  for ( int i=0; i<N; i++)
  {
    if ( input_type[i])
      colors[input_company[i]][input_color[i]][input_size[i]][0] += 1;
    else
      colors[input_company[i]][input_color[i]][input_size[i]][1] += 1;
  }
  //fill_n ( colors , 20*20*25*2 , false );


  int answer = 0;
  for (int i=0; i<index_company; i++)
    for ( int j=0; j<index_color; j++)
      for ( int k=0; k<16; k++)
        answer += min (colors[i][j][k][1],colors[i][j][k][0]);
  cout << answer << endl;
  return 0;
}
