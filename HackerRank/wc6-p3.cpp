#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
  int T; cin >> T;
  while(T--) {
    int N; cin >> N;

    int matrix[2*N][2*N];

    for(int row=0; row<2*N; row++) {
      for(int col=0; col<2*N; col++) {
        cin >> matrix[row][col];
      }
    }

    int maxValSum = 0;
    int arraySize = 2*N-1;
    for(int row=0; row<N; row++) {
      for(int col=0; col<N; col++) {
        pair<int, int> mirrHor = make_pair(row, arraySize - col);
        pair<int, int> mirrVer = make_pair(arraySize - row, col);
        pair<int, int> mirrDiag = make_pair(arraySize - row, arraySize - col);
        pair<int, int> indices = make_pair(row, col);

        int maxVal = max(matrix[mirrHor.first][mirrHor.second], matrix[mirrVer.first][mirrVer.second]);
        maxVal = max(maxVal, matrix[mirrDiag.first][mirrDiag.second]);
        maxVal = max(maxVal, matrix[indices.first][indices.second]);
        maxValSum += maxVal;
      }
    }
    cout << maxValSum << "\n";
  }
  return 0;
}
