#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> stallLocations;
int findStall(int prevStallIndex, int minDistance) {

  for(int stallIndex = prevStallIndex+1; stallIndex < stallLocations.size(); stallIndex++) {
    int curDistance = stallLocations[stallIndex] - stallLocations[prevStallIndex];
    if(curDistance >= minDistance) {
      return stallIndex;
    }
  }
  return -1;
}

bool isLengthPossible(int minDistance, int noOfCows) {
  int prevStallIndex = 0;
  noOfCows--;

  for(int cow=0; cow<noOfCows; cow++) {

    // find position for cow
    prevStallIndex = findStall(prevStallIndex, minDistance);
    if(prevStallIndex == -1) {
      return false;
    }

  }
  return true;
}


int binarySearch(int min, int max, int noOfCows) {
  while(min < max) {
    int mid = (min + max+1)/2;
    if(isLengthPossible(mid, noOfCows)) {
      min = mid;
    } else {
      max = mid-1;
    }
  }
  return min;
}

int main() {
  int noOfTestCases; cin >> noOfTestCases;

  for(int testCaseNo=0; testCaseNo<noOfTestCases; testCaseNo++) {
    int noOfStalls, noOfCows; cin >> noOfStalls >> noOfCows;
    stallLocations.clear();

    for(int stall=0; stall<noOfStalls; stall++) {
      int no; cin >> no;
      stallLocations.push_back(no);
    }
    sort(stallLocations.begin(), stallLocations.end());
    int maxDistance = stallLocations[stallLocations.size()-1] - stallLocations[0];
    int minDistance = 0;
    cout << binarySearch(minDistance, maxDistance, noOfCows) << endl;
  }
  return 0;
}
