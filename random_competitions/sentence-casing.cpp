#include <iostream>
#include <cstdio>
#include <ctype.h>
using namespace std;
int main()
{
    bool should_shift=true;
    char c='a';
    while ( scanf("%c",&c) > 0 )
    {
        if ( should_shift && c != ' ' && c != '\n')
        {
            //cout << c << "Character\n";
            if ( islower(c) )
                putchar(toupper(c));
            else
                putchar(c);
           should_shift=false;
        }
        else if ( isalpha(c) && isupper(c) )
            putchar(tolower(c));
        else
            putchar(c);

        if ( c == '.' || c == '?' || c == '!' || c == '\n')
        {
            should_shift = true;
           // cout <<"Should shift initialized\n";
        }
    }
    return 0;
}    
