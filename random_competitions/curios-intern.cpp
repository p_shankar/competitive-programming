#include <iostream>
#include <algorithm>
using namespace std;

bool if_node_leaf(int i,int l)
{
    if ( i >= (l*(l+1/2)) )
        return true;
    else
        return false;
}

int get_left_child(int i)
{
    return 2*i-2;
}

int get_right_child(int i)
{
    return 2*i-1;
}

int go_to_level(int i)
{
    return ((i*(i+1))/2-i); 
}
int get_last_node_of_level(int i)
{
    return ((i*(i+1))/2)-1;
}

int main()
{
    int L; cin >> L;
    int fun[1550]; 
    int max_fun[3000];
    for ( int i=0; i<3000; i++ )
        max_fun[i] = 0;

    for ( int i=0; i< (L*(L+1))/2; i++ )
        cin >> fun[i];
    
    for ( int i=L; i>=1; i--)
    {
       int first_node = go_to_level(i);
       int last_node = get_last_node_of_level(i);
        int sno_node=0;

       for ( int j=first_node; j<=last_node; j++ )
       {
            max_fun[j]=max(max_fun[go_to_level(i+1)+sno_node],max_fun[go_to_level(i+1)+sno_node+1])+fun[j];     
            sno_node++;
       }
    }
    cout << max_fun[0] << endl;
    return 0;
}
