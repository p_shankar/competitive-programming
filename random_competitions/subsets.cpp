#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
int bit_representation[16];
void add_one(int x)
{
    for ( int i=x-1; i>=0; i-- )
    {
        if ( bit_representation[i] == 1 )
        {
            bit_representation[i] = 0;
            continue;
        }
        else
        {
            bit_representation[i]=1;
            break;
        }
    }
}
string get_string(string a)
{
    string answer="";
    for ( int i=0; i<a.length(); i++ )
    {
        if ( bit_representation[i] == 1  )
            answer += a.at(i);
    }
    //cout << "answer:"<<answer << endl;
    return answer;
}
/*
 *  * Complete the function below.
 *   */
vector < string > buildSubsets(string s) 
{
    fill_n(bit_representation,16,0);
    vector <string> answer;
    for ( int i=1; i< pow(2,s.length()); i++ )
    {
        add_one(s.length());
        answer.push_back(get_string(s));
    }
    sort(answer.begin(),answer.end());
    return answer;
}
int main()
{
    //ofstream fout(getenv("OUTPUT_PATH"));
    vector < string > res;
    string _s;
    getline(cin, _s);

    res = buildSubsets(_s);
    for(int res_i=0; res_i < res.size(); res_i++) {
        cout << res[res_i] << endl;;
    }
    //cout << res.size();

    //fout.close();
    return 0;
}    
