#include <iostream>
using namespace std;
int main(int argc, char *argv[]) {
  int T; cin >> T; cin.get();
  for(int testCaseNo=0; testCaseNo < T; testCaseNo++) {

    int zeros=0, ones=0;
    char c;
    while( (c = cin.get()) > 0 ) {
      if(c == '\n') {
        break;
      } else if(c == '0' && zeros <2 ) {
        zeros++;
      } else if(c == '1' && ones < 2){
        ones++;
      }
    }

    if(zeros==1 || ones ==1) {
      cout << "Yes\n";
    } else {
      cout << "No\n";
    }
  }
  return 0;
}
