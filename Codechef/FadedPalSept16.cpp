#include <iostream>
using namespace std;
int main(int argc, char *argv[]) {
  int T; cin >> T;

  for(int testCaseNo=0; testCaseNo<T; testCaseNo++) {
    string fadedString; cin >> fadedString;

    size_t firstPtr = 0;
    size_t lastPtr = fadedString.length()-1;
    bool isPalindrome = true;

    while(firstPtr <= lastPtr) {
      if(fadedString[firstPtr] == fadedString[lastPtr]) {
        if(fadedString[firstPtr] == '.') {
          fadedString[firstPtr] = 'a';
          fadedString[lastPtr] = 'a';
        }
      } else if(fadedString[firstPtr] == '.' || fadedString[lastPtr] == '.') {
        if(fadedString[firstPtr] == '.') {
          fadedString[firstPtr] = fadedString[lastPtr];
        } else {
          fadedString[lastPtr] = fadedString[firstPtr];
        }
      } else {
        isPalindrome = false;
        break;
      }

      firstPtr++;
      lastPtr--;
    }

    if(isPalindrome) {
      cout << fadedString << "\n";
    } else {
      cout << "-1\n";
    }
  }
  return 0;
}
